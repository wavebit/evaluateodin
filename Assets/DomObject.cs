﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

public class ObjectValue<T2>
{
  [NonSerialized]
  private ObjectValue<T2> _parent;

  public ObjectValue<T2> Parent { get { return _parent; } set { _parent = value; } }

  [OdinSerialize]
  [OnValueChanged(nameof(OnOverriedChanged))]
  private bool _override;

  [SerializeField]
  [HideIf(memberName: nameof(NotOverride))]
  [OdinSerialize]
  private T2 _value;

  public bool Override()
  {
    return Parent == null || _override;
  }

  public bool NotOverride()
  {
    return !Override();
  }

  public void OnOverriedChanged()
  {
    //_value = Value;
  }

  [ShowIf(nameof(NotOverride))]
  [ShowInInspector, ReadOnly]
  public T2 Value
  {
    get
    {
      return Override() ? _value : Parent.Value;
    }
  }
}

[CreateAssetMenu]
public class DomObject : SerializedScriptableObject
{
  [OnValueChanged(nameof(ParentChanged))]
  [AssetsOnly]
  // prevent cyclic dependencies
  public DomObject Parent;

  //public void OnEnable()
  //{
  //  The parent property is deserizlized at this time.
  //  Debug.Log(nameof(DomObject) + nameof(OnEnable));
  //  ParentChanged();
  //}

  /// <summary>Not yet documented.</summary>
  protected override void OnAfterDeserialize()
  {
    //The parent property is not deserizlized, it should be null, unless you manual deserizlieze it.
    ParentChanged();
  }

  private void ParentChanged()
  {
    // Parent is null
    Health.Parent = Parent?.Health;
  }

  [OdinSerialize]
  public ObjectValue<int> Health = new ObjectValue<int>();
}